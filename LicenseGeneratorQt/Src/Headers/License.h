#pragma once

// ----- Includes --------------------------------------------------------------

#include "UtilString.h"
#include "Socket.h"

#include <cstring>
#include <sstream>
#include <random>

using namespace std;

// ----- Defines ---------------------------------------------------------------

#define PINCODE      10000
#define KEY			 100000

// ------ class CNodeLicense ---------------------------------------------------

class License 
{
public:
    License();
    ~License();

    // Pin Code
    string& PinCodeString() { return mPinCodeStr; };
    bool    PinCodeOk();

    // UDP Interface with controller
    void StopSocket()
	{
        delete mSocket;
        mSocket = NULL;
    }

    bool StartSocket(string inControllerAddress, int inControllerPort, int inListenPort)
	{
        StopSocket();
        
        mSocket = new SimpleUdpSocket(false, "License");
		if (!mSocket->DstAddr().SetAddressString(inControllerAddress))
		{
			//LOG_ERROR("Invalid controller address");
			return false;
		}
        mSocket->DstAddr().SetPort(inControllerPort);

        return true;
    }

    // Interface for key generation
	int     ProtocolVersion() { return mProtocolVersion;              }
    bool    HwKeyValid()      { return (mHwKeyStr.size() > 0);        }
    bool    SwKeyValid()      { return (mSwKeyStr.size() > 0);        }
    string& HwKeyString()     { return mHwKeyStr;                     }
    string& SwKeyString()     { return mSwKeyStr;                     }
	int&    HwOpHours()       { return mServoOpHours;                 }
    bool    LicenseValid()    { return mLicenseValid;                 }
    void GenerateTemporary(int inLicenseHours);
    void GeneratePermanent();

    // UDP Interface
    bool GetHardwareKey();
    bool SetSoftwareKey();
    bool SaveLicense();

protected:
	void CalculateKey(int inHours, const string& inData, string& outData);

protected:
	const int PERMANENT_LICENSE = 100000;

    string mSwLicenseKey;

    // Signals
    string  mPinCodeStr;
    string  mHwKeyStr = "", mSwKeyStr = "";
	int     mProtocolVersion;
    bool    mLicenseValid = false;
	int     mServoOpHours;
    int     mPinCode;

    SimpleUdpSocket*  mSocket = NULL; 

    //UdpMessageHwKey  mUdpMsgHwKey;
    //UdpMessageSwKey  mUdpMsgSwKey;
    //UdpMessageSave   mUdpMsgSave;

	// Random number for encryption
	std::mt19937 mRng;
	std::uniform_int_distribution<int> mDist;
};