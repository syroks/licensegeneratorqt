#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_LicenseGenerator.h"

#include "License.h"
#include "StatusLogger.h"

class LicenseGeneratorQt : public QMainWindow
{
	Q_OBJECT

public:
	LicenseGeneratorQt(QWidget *parent = 0);
	~LicenseGeneratorQt();

private slots:
	void onTextChanged();
	void onHwEditTextChanged();

	void onHwEditFocusLost();
	void onIPAddressEditFocusLost();

	void on_ButtonGetHwKey_clicked();
	void on_ButtonGenerateSwKey_clicked();
	void on_ButtonSendSwKey_clicked();
	void on_ButtonSaveSwKey_clicked();

	void on_timer_timeOut();

private:
	bool isValidValue(QString str, int minValue, int maxValue, int &outValue);
	void updateStatusGUI();
	bool updateSockets();

	void connectEvents();

	Ui::LicenseGeneratorQtClass ui;
	QString mHwKeyStr;
	License* mLicense;
	StatusLogger* mLogger;
	QTimer* mTimer;
};