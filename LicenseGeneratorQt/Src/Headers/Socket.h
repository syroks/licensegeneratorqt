#pragma once

#include <vector>
#include "Types.h"

using namespace std;

#define SOCKET_BUFFER 1472

class Parser;
class Socket;

// direct wrapper for socket addresses
class SocketAddress 
{

};

// direct wrapper for IPv4 addresses
class IPv4Address
{
public:
	// set
	void SetPort(uint16 inPort)
	{
	}

	void SetAddressLong(uint32 inAddress) 
	{
	}

	bool SetAddressString(const string& inAddress) 
	{
		return true;
	}
};

// direct wrapper for socket functions
class Socket 
{
};

// wrapper around Socket for ease of use
/*
  Please note that the Socket methods are (mostly) thread-safe, while SimpleUdpSocket methods are not:
  mSendBuffer, mRecvBuffer, mSrcAddr and mDstAddr can be changed from different threads.
*/
class SimpleUdpSocket : public Socket
{
	IPv4Address mDstAddr;
	IPv4Address mSrcAddr;

protected:
	virtual void Start() {}

public:
	explicit SimpleUdpSocket(bool inRealTime, const string& inThreadName, bool inLogErrors = true) {
		Start();
        Reserve(SOCKET_BUFFER);
    }
    void Reserve(size_t size)
	{
   
    }
    bool SendTo() 
	{
		return true;
    }
    bool RecvFrom() 
	{
		return true;
    }
    bool Bind(int inPort) 
	{
		return true;
    }

    IPv4Address& DstAddr() 
	{
        return mDstAddr;
    }
    const IPv4Address& SrcAddr() const 
	{
        return mSrcAddr;
    }

	//int SocketErrorSend() const { return mSocketErrorSend; }
	//int SocketErrorRcv()  const { return mSocketErrorRecv; }
};