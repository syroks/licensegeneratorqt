#pragma once
//#include "Logger.h"
#include <QStatusBar>

class BaseLogger {
public: 
	BaseLogger(const string& name) {}
};

enum class LogMsgType {

};

class StatusLogger : BaseLogger
{
public:
	StatusLogger(QStatusBar* statusBar) : BaseLogger("StatusLogger"), mStatusBar(statusBar) 
	{
		//Instance<Logger>().RegisterLogDevice(*this);
	}

	virtual void Log(const std::string &inEventSource, const long int inEventLine, const std::string &inEventMessage, const LogMsgType inEventMsgType, bool inFileOnly){
		mStatusBar->showMessage( QString::fromStdString( inEventMessage ) );
	}

private:
	QStatusBar* mStatusBar;
};
