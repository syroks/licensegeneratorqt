#pragma once 

#include <QLineEdit>

class TextBoxEdit : public QLineEdit
{
	Q_OBJECT

public:
	TextBoxEdit(QWidget *parent = 0);
	~TextBoxEdit();

signals:
	void lostFocus();

protected:
	virtual void focusOutEvent( QFocusEvent* event );
};
