#pragma once

#include <string>
#include <cstring>
#include <list>
#include <complex>
#include <cstdarg>
#include <vector>

using std::string;
using std::vector;

// ---- class StrUtil --------------------------------------------------------------------------------------------
//! \brief String manipulation and conversion class

class StrUtil {
public:
    static string format(const char* format, va_list args);
    static string format(const char* format, ...);

   // String Manipulation
   static string ToUpper(const string &inString);
   static void   ToUpper(string& ioString);
   static string ToLower(const string &inString);
   static void   ToLower(string& ioString);
   static string PadWithChars(string inString, char inChar, int inLength);
   
   static void RemoveLeadChars(string &ioString, const string& inChars = " ");
   static void RemoveTailChars(string &ioString, const string& inChars = " ");
   static void RemoveAllChars(string &ioString, const string& inChars);
   
   static void   RemoveAllWhitespaceInPlace(string &ioString);
   static string RemoveAllWhitespace(const string& inString);

   static bool LimitStringSize(string &ioString, int inMaxSize, int inTail=0);

   // split inString on the first occurrence of inDelimiter
   static bool SplitHead(const string& inString, string inDelimiter, string& outLeft, string& outRight);

   // split inString of the last occurrence of inDelemiter
   static bool SplitTail(const string& inString, const string& inDelimiter, string& outLeft, string& outRight);

   // returns the strings in between all the occurences of inDelimiter
   static vector<string> SplitAll(const string& inString, const string& inDelimiter);

   // adds case sensitivity to string::find
   static size_t Find  (const string& inString, const string& inSubString, size_t inOffset = 0, bool inCaseSensitive = true);

   // uses Find to replace all occurrences of inSearch in ioString with inReplace, returns true if something was replaced
   static bool Replace (string& ioString, const string& inSearch, const string& inReplace, bool inCaseSensitive = true);

   static bool StartsWithNoCase(const string& inString, const string& inPrefix);

   // uses Find
   static size_t FindChar(string inString, char inChar, size_t inOffset, bool inCaseSensitive = true);
   static bool Contains(const string& inString, const string& inSubString, bool inCaseSensitive = true);
   static bool Contains(const string& inString, const string& inSubString, size_t &outPos, bool inCaseSensitive = true);

   // uses Replace
   static bool Replace (string& ioString, const char inTheChar, const char inByChar, bool inCaseSensitive = true);
   static bool Replace (string& ioString, const char inTheChar, string inByString, bool inCaseSensitive = true);

    inline 
    static int CompareWithCase(const string &inStringA, const string &inStringB){
		return strcmp(inStringA.c_str(), inStringB.c_str());
		// return inStringA.compare(inStringB);  // disabled: this implementation is very slow
    }
    inline 
    static int CompareNoCase(const string &inStringA, const string &inStringB){
		#ifdef __LINUX__
				return strcasecmp(inStringA.c_str(), inStringB.c_str());
		#endif
		#ifdef __WINDOWS__
				return _stricmp(inStringA.c_str(), inStringB.c_str());
		#endif
		// return Compare(ToLower(inStringA), ToLower(inStringB));  // disabled: this implementation is very slow
    }
    inline
    static bool IsEqualWithCase(const string &inStringA, const string &inStringB) {
		return CompareWithCase(inStringA, inStringB) == 0;
    }
    inline
    static bool IsEqualNoCase(const string &inStringA, const string &inStringB) {
		return CompareNoCase(inStringA, inStringB) == 0;
    }
	
	inline
	static bool IsAlphanumeric(const string &inString) {
		for (auto c : inString) {
			if (!isalnum(c)) {
				return false;
			}
		}
		return true;
	}

   // Conversion
   static string ConvertVoidPtr(const void* inVoidPtr);

   static string ConvertTo(bool inBool);
   static string ConvertTo(char inChar);

   static string ConvertTo(unsigned char inChar,     string inFormatStr = "%hhu");
   static string ConvertTo(short inShort,            string inFormatStr = "%hd" );
   static string ConvertTo(unsigned short inShort,   string inFormatStr = "%hu" );
   static string ConvertTo(int inInt,                string inFormatStr = "%d"  );
   static string ConvertTo(unsigned int inInt,       string inFormatStr = "%u"  );
   static string ConvertTo(long inLong,              string inFormatStr = "%ld" );
   static string ConvertTo(unsigned long inLong,     string inFormatStr = "%lu" );
   static string ConvertTo(long long inInt,          string inFormatStr = "%lld");
   static string ConvertTo(unsigned long long inInt, string inFormatStr = "%llu");
   static string ConvertTo(float inFloat,            string inFormatStr = "%+g" );
   static string ConvertTo(double inDouble,          string inFormatStr = "%+lg");

   static string ConvertTo(const string  &inString);
   static string ConvertTo(const string* &inString);
   static string ConvertTo(      string*  inString);
   static string ConvertTo(const char    *inString);
   					    
   static bool ConvertFrom(const string &inString, bool &outBool);
   static bool ConvertFrom(const string &inString, char& outChar);
   static bool ConvertFrom(const string &inString, unsigned char& outChar);
   static bool ConvertFrom(const string &inString, short &outShort);
   static bool ConvertFrom(const string &inString, unsigned short &outShort);
   static bool ConvertFrom(const string &inString, int &outInt);
   static bool ConvertFrom(const string &inString, int &outInt, string inFormatStr);
   static bool ConvertFrom(const string &inString, unsigned int &outInt);
   static bool ConvertFrom(const string &inString, unsigned int &outInt, string inFormatStr);
   static bool ConvertFrom(const string &inString, long &outLong);
   static bool ConvertFrom(const string &inString, unsigned long &outLong);
   static bool ConvertFrom(const string &inString, float &outFloat);
   static bool ConvertFrom(const string &inString, double &outDouble);
   static bool ConvertFrom(const string &inString, string &outString);

   /* list<string> tmp = {"1", "2", "3"};
      intercalate(", ", tmp.begin(), tmp.end()) == "1, 2, 3"
      */
   template <class StringInputIterator>
   static string intercalate(const string &inSeperator, StringInputIterator begin, StringInputIterator end){
       string result;

       if (begin == end){
           return result;
       }

       result.append(*begin);
       begin++;

       while(begin != end){
           result.append(inSeperator);
           result.append(*begin);
           begin++;
       }
       
       return result;
   }

   // Conversion between string and wstring: ref http://social.msdn.microsoft.com/Forums/en-US/Vsexpressvc/thread/0f749fd8-8a43-4580-b54b-fbf964d68375
   #ifdef __WIN32__ 
	#include <xstring>
	static std::wstring s2ws(const string& s);
	static string       ws2s(const std::wstring& ws);
   #endif

    static string CreateComponentName(const string& inString);
};

