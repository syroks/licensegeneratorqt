#include "License.h"
//#include "UtilsNumeric.h"
//#include "UtilsBit.h"
//#include "Crypto.h"
#include <QString>

// ------ class License ------------------------------------------------------------------------

License::License() 
{
	// Random 
	std::random_device rd;    
	mRng = std::mt19937(rd());   
	mDist = std::uniform_int_distribution<int>(100000, 999999);
}

License::~License() 
{
};

bool License::PinCodeOk() 
{
    // Check pin code
	if ((StrUtil::ConvertFrom(mPinCodeStr, mPinCode)) && (mPinCode == PINCODE))
	{
		return true;
    }
    else return false;
}

void License::GenerateTemporary(int inLicenseHours) 
{
    // Default
    mSwKeyStr = "";

    // Check pin code
	if (!PinCodeOk()) return;

	CalculateKey(inLicenseHours + mServoOpHours, mHwKeyStr, mSwKeyStr);
}

void License::GeneratePermanent() 
{
    // Default
    mSwKeyStr = "";

    // Check pin code
	if (!PinCodeOk()) return;

	CalculateKey(PERMANENT_LICENSE, mHwKeyStr, mSwKeyStr);
}

// UDP Interface
bool License::GetHardwareKey() 
{
    // Send a hw key request
	//mUdpMsgHwKey.Import(0, 0, false, "");
    //mUdpMsgHwKey.ExportToSocket(*mSocket);
    mSocket->SendTo();

    mSwKeyStr = "";
	for (int i = 0; i < 200; i++)
	{
		if (mSocket->RecvFrom()) 
		{
			// Check for Hw key messages
			//if (mUdpMsgHwKey.Validate(*mSocket)) 
			//{
				// Get the Hardware Key
				//mUdpMsgHwKey.ImportFromSocket(*mSocket);
				//mUdpMsgHwKey.Export(mProtocolVersion, mServoOpHours, mLicenseValid, mHwKeyStr);
				//return true;
			//}
		}
	}
    return false;
}

bool License::SetSoftwareKey() 
{
    // Send a sw key to the controller
   // mUdpMsgSwKey.Import(mSwKeyStr);
    //mUdpMsgSwKey.ExportToSocket(*mSocket);
    mSocket->SendTo();

	for (int i = 0; i < 200; i++)
	{
		if (mSocket->RecvFrom()) 
		{
			// Check for Hw key messages
			//if (mUdpMsgHwKey.Validate(*mSocket)) 
			//{
				// Get the Hardware Key
				//mUdpMsgHwKey.ImportFromSocket(*mSocket);
				//mUdpMsgHwKey.Export(mProtocolVersion, mServoOpHours, mLicenseValid, mHwKeyStr);
				//return true;
			//}
		}
	}
    return false;
}

bool License::SaveLicense()
{
    // Send a sw key to the controller
   // mUdpMsgSave.Import();
    //mUdpMsgSave.ExportToSocket(*mSocket);
    return mSocket->SendTo();
}

// --- protected methods ---

void License::CalculateKey(int inHours, const string& inData, string& outData)
{
	//key caculation logic
}