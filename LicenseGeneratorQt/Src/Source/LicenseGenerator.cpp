#include "stdafx.h"
#include "LicenseGenerator.h"
#include "TextBoxEdit.h"

LicenseGeneratorQt::LicenseGeneratorQt(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	this->setWindowIcon(QIcon(":/LicenseGeneratorQt/appIcon"));
	connectEvents();

	//InitNetwork();

	mLogger = new StatusLogger(ui.statusBar);
	//LOG_MESSAGE("License Generator started");
	mLicense = new License;

	mTimer = new QTimer(this);
	mTimer->start(1000);
	QObject::connect(mTimer, SIGNAL(timeout()), this, SLOT(on_timer_timeOut()));
}

LicenseGeneratorQt::~LicenseGeneratorQt()
{

}

bool LicenseGeneratorQt::isValidValue(QString str, int minValue, int maxValue, int &outValue)
{
	// Convert Value and check validity and boundaries
	if (!StrUtil::ConvertFrom(str.toStdString(), outValue))
	{
		outValue = 0;
		return false;
	}

	// Check positive boundary
	if ((outValue < minValue) | (outValue > maxValue)) 
	{
		return false;
	}
	return true;
}

void LicenseGeneratorQt::updateStatusGUI()
{
	mLicense->HwKeyString() = ui.textEditHwKey->text().toStdString();
	mLicense->SwKeyString() = ui.textEditLicenseKey->text().toStdString();
	mLicense->PinCodeString() = ui.textEditPinCode->text().toStdString();

	ui.textEditHwKey->setEnabled( mLicense->PinCodeOk() );
	ui.textEditLicenseKey->setEnabled( mLicense->PinCodeOk() );
	ui.textEditIPAddress->setEnabled( mLicense->PinCodeOk() );
	ui.textEditPort->setEnabled(mLicense->PinCodeOk());
	ui.textEditPinCode->setEnabled(!mLicense->PinCodeOk());
	ui.textEditHwHours->setEnabled(mLicense->PinCodeOk());

	ui.ButtonGenerateSwKey->setEnabled(mLicense->PinCodeOk() && mLicense->HwKeyValid());
	ui.ButtonGetHwKey->setEnabled(mLicense->PinCodeOk());
	ui.ButtonSaveSwKey->setEnabled(mLicense->PinCodeOk() && mLicense->HwKeyValid() && mLicense->SwKeyValid() && mLicense->LicenseValid());
	ui.ButtonSendSwKey->setEnabled(mLicense->PinCodeOk() && mLicense->HwKeyValid() && mLicense->SwKeyValid());

	ui.radioButtonPermanent->setEnabled(mLicense->PinCodeOk());
	ui.radioButtonTemporary->setEnabled(mLicense->PinCodeOk());
	ui.labelOperationalHours->setEnabled(mLicense->PinCodeOk() && ui.radioButtonTemporary->isChecked());
	ui.textEditOperationalHours->setEnabled(mLicense->PinCodeOk() && ui.radioButtonTemporary->isChecked());

}

bool LicenseGeneratorQt::updateSockets()
{
	int controllerPort;
	if (!isValidValue(ui.textEditPort->text(), 1000, 7000, controllerPort))
	{
		//LOG_ERROR("Invalid controller port");
		return false;
	}

	int listeningPort = 0;
	string controllerAddress = ui.textEditIPAddress->text().toStdString();
	if (!mLicense->StartSocket(controllerAddress, controllerPort, listeningPort))
	{
		return false;
	}

	return true;
}

void LicenseGeneratorQt::connectEvents()
{
	connect(ui.textEditPinCode, &QLineEdit::textChanged, this, &LicenseGeneratorQt::onTextChanged);
	connect(ui.textEditLicenseKey, &QLineEdit::textChanged, this, &LicenseGeneratorQt::onTextChanged);
	connect(ui.textEditHwKey, &QLineEdit::textChanged, this, &LicenseGeneratorQt::onTextChanged);
	connect(ui.textEditHwKey, &QLineEdit::textChanged, this, &LicenseGeneratorQt::onHwEditTextChanged);
	
	connect(ui.textEditHwKey, &TextBoxEdit::lostFocus, this, &LicenseGeneratorQt::onHwEditFocusLost);
	connect(ui.textEditIPAddress, &TextBoxEdit::lostFocus, this, &LicenseGeneratorQt::onIPAddressEditFocusLost);

	connect(ui.radioButtonPermanent, &QRadioButton::clicked, this, &LicenseGeneratorQt::onTextChanged);
	connect(ui.radioButtonTemporary, &QRadioButton::clicked, this, &LicenseGeneratorQt::onTextChanged);

	connect(ui.textEditHwHours, &QLineEdit::textChanged, [&](){ mLicense->HwOpHours() = ui.textEditHwHours->text().toInt(); });
}

//Slots

void LicenseGeneratorQt::onTextChanged()
{
	updateStatusGUI();
}

void LicenseGeneratorQt::onHwEditTextChanged()
{
	mLicense->HwKeyString() = ui.textEditHwKey->text().toStdString();
}

void LicenseGeneratorQt::onHwEditFocusLost()
{
	mLicense->HwKeyString() = ui.textEditHwKey->text().toStdString();
}

void LicenseGeneratorQt::onIPAddressEditFocusLost()
{
	auto text = ui.textEditIPAddress->text().trimmed();
	// selection of local host
	if (text == "0") {
		text = "127.0.0.1";
	}
	else if (text == "1") {
		text = "10.1.90.18";
	}
	else if (text == "2") {
		text = "10.1.92.18";
	}
	else if (text == "3") {
		text = "192.168.5.5";
	}
	ui.textEditIPAddress->setText(text);
}

void LicenseGeneratorQt::on_ButtonGetHwKey_clicked()
{
	if ( !updateSockets() ) return;
	if ( !mLicense->GetHardwareKey() )
	{
		//LOG_ERROR("Controller response time-out. Check IP/port or enter key manually.");
		return;
	}

	// Check protocol
	if (mLicense->ProtocolVersion() > 2) {
		//Show message
		QMessageBox msgBox;
		msgBox.setText("This license generator is not compatible with the connected controller. Please use a newer version.");
		msgBox.exec() ;
		return;
	}

	ui.textEditHwKey->setText(QString::fromStdString(mLicense->HwKeyString()));
	ui.textEditHwHours->setText(QString::number(mLicense->HwOpHours()));
	ui.textEditLicenseKey->setText(QString::fromStdString(mLicense->SwKeyString()));
	//LOG_MESSAGE("Retrieved hardware key");
}

void LicenseGeneratorQt::on_ButtonGenerateSwKey_clicked()
{
	if (ui.radioButtonTemporary->isChecked())
	{
		// Check current hours
		bool converted = false;
		int currentHours = ui.textEditHwHours->text().toInt(&converted);
		if (!converted || currentHours <= 0)
		{
			//Show message
			QMessageBox msgBox;
			msgBox.setText("Please, fill in the current operating hours.");
			msgBox.exec() ;
			return;
		}

		// Retrieve remaining hours
		converted = false;
		int hours = ui.textEditOperationalHours->text().toInt(&converted);
		if (!converted)
		{
			//Show message
			QMessageBox msgBox;
			msgBox.setText("Please, fill in a valid amount of hours remaining.");
			msgBox.exec() ;
			return;
		}
		mLicense->GenerateTemporary(hours);
	} 
	else {
		mLicense->GeneratePermanent();
	}

	ui.textEditLicenseKey->setText(QString::fromStdString(mLicense->SwKeyString()));
	//LOG_MESSAGE("Generated license key");
}

void LicenseGeneratorQt::on_ButtonSendSwKey_clicked()
{
	if ( !updateSockets() ) return;
	if ( !mLicense->SetSoftwareKey() )
	{
		//LOG_ERROR("Failed to send software key");
		return;
	}
	ui.textEditHwKey->setText(QString::fromStdString(mLicense->HwKeyString()));
	ui.textEditHwHours->setText(QString::number(mLicense->HwOpHours()));
	ui.textEditLicenseKey->setText(QString::fromStdString(mLicense->SwKeyString()));
	if (mLicense->LicenseValid()) 
	{
		//LOG_MESSAGE("License is valid");
	}
	else 
	{
		//LOG_MESSAGE("License not valid");
	}
}

void LicenseGeneratorQt::on_ButtonSaveSwKey_clicked()
{
	if ( !updateSockets() ) return;
	if ( !mLicense->SaveLicense() )
	{
		//LOG_ERROR("Failed to save license key");
		return;
	}
	//LOG_MESSAGE("Saved license key");
}

void LicenseGeneratorQt::on_timer_timeOut()
{
	updateStatusGUI();
}