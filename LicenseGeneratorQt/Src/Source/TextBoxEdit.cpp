#include "TextBoxEdit.h"

TextBoxEdit::TextBoxEdit(QWidget *parent)
	: QLineEdit(parent)
{
}

TextBoxEdit::~TextBoxEdit()
{
}

//Protected

void TextBoxEdit::focusOutEvent(QFocusEvent* event)
{
	emit lostFocus();
	QLineEdit::focusOutEvent(event);
}
