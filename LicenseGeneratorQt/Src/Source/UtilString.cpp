// ----- Includes --------------------------------------------------------------

#include "UtilString.h"

#include <stdio.h>
#include <ctype.h>
#include <float.h>
#include <string.h>
#include <time.h>
#include <stdio.h>
#include <sstream>
#include <algorithm>

#ifdef __WIN32__ 
	#include <windows.h>
#endif

#ifdef __LINUX__
    #define stricmp strcasecmp
#endif

using namespace std;

// ----- String Utils ---------------------------------------------------------

// String Manipulation
string StrUtil::ToUpper(const string &inString) {
    string result = inString;
    ToUpper(result);
    return result;
}

void StrUtil::ToUpper(string& ioString)
{
    for (size_t i = 0; i < ioString.length (); i++) {
        ioString[i] = static_cast<char>(toupper(ioString[i]));
    }
}

string StrUtil::ToLower(const string &inString) {
    string result = inString;
    ToLower(result);
    return result;
}

void StrUtil::ToLower(string& ioString)
{
    for (size_t i = 0; i < ioString.length (); i++) {
        ioString[i] = static_cast<char>(tolower(ioString[i]));
    }
}

string StrUtil::PadWithChars(string inString, char inChar, int inLength) {
   for (int i=(int)inString.size(); i<inLength; i++) {
      inString.push_back(inChar);
   }
   return inString;
}

void StrUtil::RemoveLeadChars(string &ioString, const string& inChars)
{
    size_t index = ioString.find_first_not_of(inChars);
    if (index == string::npos){
        ioString.clear();
    } else {
        ioString.erase(0, index);
    }
}

void StrUtil::RemoveTailChars(string &ioString, const string& inChars)
{
    size_t index = ioString.find_last_not_of(inChars);
    if (index == string::npos){
        ioString.clear();
    } else {
        ioString.erase(index+1, string::npos);
    }
}

void StrUtil::RemoveAllWhitespaceInPlace(string &ioString) {
    RemoveAllChars(ioString, " \t\n\r");
}

string StrUtil::RemoveAllWhitespace(const string& inString){
    string result = inString;
    RemoveAllWhitespaceInPlace(result);
    return result;
}

bool StrUtil::LimitStringSize(string &ioString, int inMaxSize, int inTail) {
    if ( int(ioString.size()) > inMaxSize) {
        ioString.resize(inMaxSize);
        if (inTail>0) {
            ioString.resize(inMaxSize+inTail,'.');
        }
        return true;
    }
    return false;
}

bool StrUtil::SplitHead(const string& inString, string inDelimiter, string& outLeft, string& outRight) {
   if (!inString.empty() && !inDelimiter.empty()) {
      size_t pos = inString.find(inDelimiter);
      if (pos != string::npos) {
         // Success
         outLeft  = inString.substr (0, pos);
         outRight = inString.substr (pos + inDelimiter.size());
         return true;
      }
   }
   // delimiter string not found or the input string is empty
   outLeft  = "";
   outRight = inString;
   return false;
}

bool StrUtil::SplitTail(const string& inString, const string& inDelimiter, string& outLeft, string& outRight) {
   if (!inString.empty() && !inDelimiter.empty()) {
      size_t pos = inString.rfind(inDelimiter);
      if (pos != string::npos) {
         // Success
         outLeft  = inString.substr (0, pos);
         outRight = inString.substr (pos + inDelimiter.size());
         return true;
      }
   }
   // delimiter string not found or the input string is empty
   outLeft  = inString;
   outRight = "";
   return false;
}

bool StrUtil::Replace( string& ioString, const string& inSearch, const string& inReplace, bool inCaseSensitive /*= true*/ )
{
    bool result = false;
    size_t offset = 0;
    size_t position;
    while((position = Find(ioString, inSearch, offset, inCaseSensitive)) != string::npos){
        ioString.replace(position, inSearch.length(), inReplace);
        offset = position + inReplace.length();
        result = true;
    }
    return result;
}

bool StrUtil::Replace( string& ioString, const char inTheChar, const char inByChar, bool inCaseSensitive /*= true*/ )
{
    return Replace(ioString, string(1, inTheChar), string(1, inByChar), inCaseSensitive);
}

bool StrUtil::Replace( string& ioString, const char inTheChar, string inByString, bool inCaseSensitive /*= true*/ )
{
    return Replace(ioString, string(1, inTheChar), inByString, inCaseSensitive);
}

// ----- Conversion of standard types into string -----------------------------

string StrUtil::ConvertVoidPtr(const void* inVoidPtr) {
    // Convert address referenced by void* to 4 byte string
    char theBuffer[20];
    sprintf(theBuffer, "0x%p", inVoidPtr);
    return string(theBuffer);
}

string StrUtil::ConvertTo( bool inBool) {
    string outString;
    if (!inBool) {
      outString = "false";
   } else {
      outString = "true";
   };
   return outString;
}

string StrUtil::ConvertTo( char inChar) {
    string outString;
    outString.assign (1, inChar);
    return outString;
}

string StrUtil::ConvertTo( unsigned char inChar, string inFormatStr){
    char theBuffer[128]; 
    sprintf(theBuffer, inFormatStr.c_str(), inChar); 
    string outString = theBuffer; 
    return outString;
}
    

string StrUtil::ConvertTo(short inShort, string inFormatStr) {
    char theBuffer[128]; 
    sprintf(theBuffer, inFormatStr.c_str(), inShort); 
    string outString = theBuffer; 
    return outString;
}

string StrUtil::ConvertTo(unsigned short inShort, string inFormatStr) {
    char theBuffer[128]; 
    sprintf(theBuffer, inFormatStr.c_str(), inShort); 
    string outString = theBuffer; 
    return outString;
}

string StrUtil::ConvertTo(int inInt, string inFormatStr) {
    char theBuffer[128]; 
    sprintf(theBuffer, inFormatStr.c_str(), inInt); 
    string outString = theBuffer; 
    return outString;
}

string StrUtil::ConvertTo(unsigned int inInt, string inFormatStr) {
    char theBuffer[128]; 
    sprintf(theBuffer, inFormatStr.c_str(), inInt); 
    string outString = theBuffer; 
    return outString;
}

string StrUtil::ConvertTo(long inLong, string inFormatStr) {
    char theBuffer[128]; 
    sprintf(theBuffer, inFormatStr.c_str(), inLong); 
    string outString = theBuffer; 
    return outString;
}

string StrUtil::ConvertTo(unsigned long inLong, string inFormatStr) {
    char theBuffer[128]; 
    sprintf(theBuffer, inFormatStr.c_str(), inLong); 
    string outString = theBuffer; 
    return outString;
}


string StrUtil::ConvertTo(long long inInt, string inFormatStr) {
    char theBuffer[128]; 
    sprintf(theBuffer, inFormatStr.c_str(), inInt); 
    string outString = theBuffer; 
    return outString;
}

string StrUtil::ConvertTo(unsigned long long inInt, string inFormatStr) {
    char theBuffer[128]; 
    sprintf(theBuffer, inFormatStr.c_str(), inInt); 
    string outString = theBuffer; 
    return outString;
}

string StrUtil::ConvertTo( float inFloat, string inFormatStr) {
    char theBuffer[128];
    sprintf(theBuffer, inFormatStr.c_str(), inFloat);
    string outString = theBuffer; 
    return outString;
}

string StrUtil::ConvertTo( double inDouble, string inFormatStr) {
   char theBuffer[128];
   sprintf(theBuffer, inFormatStr.c_str(), inDouble);
   string outString = theBuffer; 
   return outString;
}

string StrUtil::ConvertTo( const string &inString) {
   string outString = inString;
   return outString;
}

string StrUtil::ConvertTo( const string* &inString) {
   string outString = *inString;
   return outString;
}

string StrUtil::ConvertTo( string* inString) {
   string outString = *inString;
   return outString;
}

string StrUtil::ConvertTo( const char *inString) {
    string outString = inString;
   return outString;
}

// ----------------------------------------------------------------------------

bool StrUtil::ConvertFrom( const string &inString, bool &outBool) {
    if ((StrUtil::IsEqualNoCase(inString, "t")) || (StrUtil::IsEqualNoCase(inString, "true")) ) {
		outBool = true;
		return true;
	}
	if ((StrUtil::IsEqualNoCase(inString, "f")) || (StrUtil::IsEqualNoCase(inString, "false")) ) {
		outBool = false;
		return true;
	}
    return false;
}

bool StrUtil::ConvertFrom( const string &inString, char &outChar) {
  return (sscanf(inString.c_str (), "%c", &outChar) == 1);
}

bool StrUtil::ConvertFrom( const string &inString, unsigned char &outChar) {
    unsigned short uShort;
    if (sscanf(inString.c_str (), "%hu", &uShort) == 1) {
        outChar = (unsigned char) uShort;
        return true;
    }
    return false;
}

bool StrUtil::ConvertFrom( const string &inString, short &outShort) {
    return (sscanf(inString.c_str (), "%hd", &outShort) == 1);
}

bool StrUtil::ConvertFrom( const string &inString, unsigned short &outShort) {
    return (sscanf(inString.c_str (), "%hu", &outShort) == 1);
}

bool StrUtil::ConvertFrom( const string &inString, int &outInt) {
    return (sscanf(inString.c_str (), "%d", &outInt) == 1);
}

bool StrUtil::ConvertFrom( const string &inString, int &outInt, string inFormatStr) {
    return (sscanf(inString.c_str (), inFormatStr.c_str(), &outInt) == 1);
}

bool StrUtil::ConvertFrom( const string &inString, unsigned int &outInt) {
    return (sscanf(inString.c_str (), "%u", &outInt) == 1);
}

bool StrUtil::ConvertFrom( const string &inString, unsigned int &outInt, string inFormatStr) {
    return (sscanf(inString.c_str (), inFormatStr.c_str(), &outInt) == 1);
}

bool StrUtil::ConvertFrom( const string &inString, unsigned long &outLong) {
    return (sscanf(inString.c_str (), "%lu", &outLong) == 1);
}

bool StrUtil::ConvertFrom( const string &inString, long &outLong) {
    return (sscanf(inString.c_str (), "%ld", &outLong) == 1);
}

bool StrUtil::ConvertFrom( const string &inString, float &outFloat) {
    return (sscanf(inString.c_str (), "%g", &outFloat) == 1);
}

bool StrUtil::ConvertFrom( const string &inString, double &outDouble) {
    return (sscanf(inString.c_str (), "%lg", &outDouble) == 1);
}

bool StrUtil::ConvertFrom( const string &inString, string &outString) {
    outString = inString;
    return true;
}

// Conversion between string and wstring: ref http://social.msdn.microsoft.com/Forums/en-US/Vsexpressvc/thread/0f749fd8-8a43-4580-b54b-fbf964d68375
#ifdef __WIN32__ 

   wstring StrUtil::s2ws(const std::string& s) {
       int slength = (int)s.length() + 1;
       int len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0); 
       wchar_t* buf = new wchar_t[len];
       MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
       wstring r(buf);
       delete[] buf;
       return r;
   }

   string StrUtil::ws2s(const std::wstring& ws) {
       int slength = (int)ws.length() + 1;
       int len = WideCharToMultiByte(CP_ACP, 0, ws.c_str(), slength, 0, 0, 0, 0); 
       char* buf = new char[len];
       WideCharToMultiByte(CP_ACP, 0, ws.c_str(), slength, buf, len, 0, 0); 
       string r(buf);
       delete[] buf;
       return r;
   }

#endif

string StrUtil::CreateComponentName(const string& inString) {
	ostringstream result;
	for (string::const_iterator it = inString.begin(); it != inString.end(); it++)
		if (*it == '.') {
			result << '~';
		} else if (isalnum(*it) == 0) {
			result << '_';
		} else {
			result << *it;
		}
	return result.str();
}

bool cmp(char inCh1, char inCh2){
    return toupper(inCh1) == toupper(inCh2);
}

size_t StrUtil::Find( const string& inString, const string& inSubString, size_t inOffset /*= 0*/, bool inCaseSensitive /*= true*/ )
{
    if (inCaseSensitive){
        return inString.find(inSubString, inOffset);
    } else {
        string::const_iterator pos = std::search(inString.begin() + inOffset, inString.end(), inSubString.begin(), inSubString.end(), cmp);
        if (pos != inString.end()){
            return pos - inString.begin();
        } else {
            return string::npos;
        }
    }
}

size_t StrUtil::FindChar( string inString, char inChar, size_t inOffset, bool inCaseSensitive /*= true*/ )
{
    return Find(inString, string(1, inChar), inOffset, inCaseSensitive);
}

bool StrUtil::Contains( const string& inString, const string& inSubString, bool inCaseSensitive /*= true*/ )
{
    size_t tmpPos;
    return Contains(inString, inSubString, tmpPos, inCaseSensitive);
}

bool StrUtil::Contains( const string& inString, const string& inSubString, size_t &outPos, bool inCaseSensitive /*= true*/ )
{
    return (outPos = Find(inString, inSubString, 0, inCaseSensitive)) != string::npos;
}

std::string StrUtil::format( const char* format, ... )
{
    va_list args;
    va_start(args, format);
    string result = StrUtil::format(format, args);
    va_end(args);
    return result;
}

#ifndef va_copy
    #define va_copy(d,s) ((d) = (s))
#endif

std::string StrUtil::format( const char* format, va_list args )
{
	string result;
    va_list tmp;
    va_copy(tmp, args);
	size_t size = vsnprintf(NULL, 0, format, tmp);
	va_end(tmp);
    if (size == 0){
        return result;   
    }
    // c++ strings are not guaranteed to be contiguous in memory, but in practice this is always the case
    result.resize(size);
    vsnprintf(&result[0], size+1, format, args);
    return result;
}

vector<string> StrUtil::SplitAll( const string& inString, const string& inDelimiter )
{
    vector<string> result;
    string input = inString;
    string left, right;
    while(SplitHead(input, inDelimiter, left, right)){
        result.push_back(left);
        input = right;
    }
    result.push_back(input);
    return result;
}

void StrUtil::RemoveAllChars(string &ioString, const string& inChars)
{
    string::iterator last = ioString.end();
    for(string::const_iterator each = inChars.begin(); each != inChars.end(); each++){
        last = std::remove(ioString.begin(), last, *each);
    }
    ioString.erase(last, ioString.end());
}

bool StrUtil::StartsWithNoCase(const string& inString, const string& inPrefix)
{
    return IsEqualNoCase(inString.substr(0, inPrefix.size()), inPrefix);
}
